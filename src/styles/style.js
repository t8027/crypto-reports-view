import { makeStyles } from "@mui/styles";

export default makeStyles((theme) => ({
  buttonBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    width: "50%",
    margin: "auto",
    flexDirection: "column",
    "& Button": {
      width: "20%",
      marginTop: 20,
    },
    "@media (max-width: 780px)": {
    },
  },
  dashboard: {
    display: "flex",
    "@media (max-width: 780px)": {
      display: "block",
    },
  },
  dashboardItem: {
    width: "50%",
  },
  radiobox: {
    display: "flex",
    flexDirection: "row",
  },
  singleChart: {
    margin: "auto",
  },
  chartDetail: {
    display: "flex",
    alignItems: "center",
    width: "80%",
  },
  max: {
    color: "#82ca9d",
  },
  min: {
    color: "red",
  },
  pairChart: {
    marginTop: 40,
  },
  whitebox: {
    background: "#fff",
    borderRadius: 20,
    width: "80%",
    height: "80%",
  },
}));
