import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCryptoPair } from "../redux/features/cryptoSlice";
import { getCryptoSingle } from "../redux/features/cryptoSingleSlice";
import {
  BarChart,
  Bar,
  Tooltip,
  XAxis,
  YAxis,
  CartesianGrid,
  Legend,
} from "recharts";
import { Button } from "@mui/material";
import useStyle from "../styles/style";

const PairChart = () => {
  const { cryptoPairList, loading } = useSelector((state) => state.cryptoPair);
  const { cryptoSingleList } = useSelector((state) => state.cryptoSingle);
  const dispatch = useDispatch();
  const [statusName, setStatusName] = useState("Higher Lower average");
  const [max, setMax] = useState(0);
  const [min, setMin] = useState(0);
  const [mergeData, setMergeData] = useState([]);

  const calculateMax = () => {
    var HighList = [];
    cryptoPairList.map((item) => HighList.push(item.high));
    setMax(Math.max(...HighList));
  };

  const calculateMin = () => {
    var LowList = [];
    cryptoPairList.map((item) => LowList.push(item.low));
    setMin(Math.min(...LowList));
  };


  // handle action of show and hide chart bar after click buttons
  var barClassName = "Higher Lower average";
  const handleShow = (text) => {
    setStatusName(text);
    if (text === "Higher") {
      barClassName = "Higher";
      setStatusName("Higher");
    } else if (text === "Lower") {
      barClassName = "Lower";
      setStatusName("Lower");
    } else if (text === "average") {
      barClassName = "average";
      setStatusName("average");
    } else {
      barClassName = "Higher Lower average";
      setStatusName("Higher Lower average");
    }
  };

  useEffect(() => {
    dispatch(getCryptoPair());
    dispatch(getCryptoSingle());
    // Save API in localStorage
    localStorage.setItem("singleAPI", JSON.stringify(cryptoSingleList));
    localStorage.setItem("PairAPI", JSON.stringify(cryptoPairList));
  }, []);

  // Calculate max & min after data fetch
  useEffect(() => {
    if (cryptoPairList.length > 0) {
      calculateMax();
      calculateMin();
    }
  }, [cryptoPairList]);

  // Merge item of 2 api's that have Same Time
  useEffect(() => {
    let totalData = cryptoSingleList.map((item, i) =>
      Object.assign({}, item, cryptoPairList[i])
    );
    setMergeData(totalData);
  }, [cryptoPairList, cryptoSingleList]);

  const classes = useStyle();
  return (
    <div>
      <BarChart
        className={classes.pairChart}
        width={730}
        height={250}
        data={mergeData}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />

        <Bar
          style={{
            display: statusName.includes("Higher") ? "block" : "none",
          }}
          dataKey="high"
          fill="#82ca9d"
        />
        <Bar
          style={{
            display: statusName.includes("Lower") ? "block" : "none",
          }}
          dataKey="low"
          fill="red"
        />
        <Bar
          style={{
            display: statusName.includes("average") ? "block" : "none",
          }}
          dataKey="volume"
          fill="orange"
        />
      </BarChart>
      <div>
        <div className={classes.chartDetail}>
          <div className={classes.buttonBox}>
            <Button
              variant="contained"
              color="success"
              onClick={() => handleShow("Higher")}
            >
              Higher
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={() => handleShow("Lower")}
            >
              Lower
            </Button>

            <Button
              variant="contained"
              color="warning"
              onClick={() => handleShow("average")}
            >
              Vol
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => handleShow("Higher Lower average")}
            >
              All
            </Button>
          </div>
          <p className={classes.max}>Maximum Range: {max}</p>
          <p className={classes.min}>Minimum Range: {min}</p>
        </div>
      </div>
    </div>
  );
};

export default PairChart;
