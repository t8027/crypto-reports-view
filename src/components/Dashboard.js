import React from "react";
import ExchangeChart from "./ExchangeChart";
import PairChart from "./PairChart";
import useStyle from "../styles/style";

const Dashboard = () => {
  const classes = useStyle();
  return (
    <div className={classes.dashboard}>
      <PairChart className={classes.dashboardItem} />
      <ExchangeChart className={classes.dashboardItem} />
    </div>
  );
};

export default Dashboard;
