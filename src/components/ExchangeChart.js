import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCryptoSingle } from "../redux/features/cryptoSingleSlice";
import {
  BarChart,
  Bar,
  Tooltip,
  XAxis,
  YAxis,
  CartesianGrid,
  Legend,
} from "recharts";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import useStyle from "../styles/style";

const ExchangeChart = () => {
  const { cryptoSingleList, loading } = useSelector(
    (state) => state.cryptoSingle
  );
  const dispatch = useDispatch();

  const [selectedItem, setSelectedItem] = useState([]);

  useEffect(() => {
    dispatch(getCryptoSingle());
  }, []);

  useEffect(() => {
    setSelectedItem(cryptoSingleList[0]);
  }, [cryptoSingleList]);

  useEffect(() => {
  }, [selectedItem]);

  const handleSelect = (item) => {
    setSelectedItem(item);
  };
  const classes = useStyle();
  return (
    <div>
      <div>
        Market volume of
        {selectedItem ? (
          <div>
            {new Intl.DateTimeFormat("en-US", {
              year: "numeric",
              month: "2-digit",
              day: "2-digit",
              hour: "2-digit",
              minute: "2-digit",
              second: "2-digit",
            }).format(selectedItem.time)}
          </div>
        ) : (
          ""
        )}
      </div>
      <BarChart
        className={classes.singleChart}
        width={330}
        height={250}
        data={[selectedItem]}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="time" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="volume" fill="#82ca9d" barSize={20} />
      </BarChart>

      <FormControl>
        <div className={classes.radiobox}>
          <RadioGroup
            aria-labelledby="demo-radio-buttons-group-label"
            name="radio-buttons-group"
          >
            {cryptoSingleList.map((item) => (
              <p className="title-box" key={item.time}>
                <FormControlLabel
                  onClick={() => handleSelect(item)}
                  value={item.volume}
                  control={<Radio />}
                  checked={item === selectedItem}
                />
              </p>
            ))}
          </RadioGroup>
        </div>
      </FormControl>
    </div>
  );
};

export default ExchangeChart;
