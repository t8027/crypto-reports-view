import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const getCryptoPair = createAsyncThunk(
  "cryptoPair/getCryptoPair",
  async () => {
    return fetch(
      "https://min-api.cryptocompare.com/data/v2/histohour?fsym=BTC&tsym=USD&limit=10"
    ).then((res) => res.json());
  }
);


const cryptoPairSlice = createSlice({
  name: "cryptoPair",
  initialState: {
    cryptoPairList: [],
    loading: false,
  },
  extraReducers: {
    [getCryptoPair.pendding]: (state, action) => {
      state.loading = true;
    },
    [getCryptoPair.fulfilled]: (state, action) => {
      state.loading = false;
      state.cryptoPairList = action.payload.Data.Data;
      console.log(action.payload.Data.Data);
    },
    [getCryptoPair.rejected]: (state, action) => {
      state.loading = false;
    },
  },
});
export default cryptoPairSlice.reducer;
