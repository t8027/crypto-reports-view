import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const getCryptoSingle = createAsyncThunk(
  "cryptoSingle/getCryptoSingle",
  async () => {
    return fetch(
      "https://min-api.cryptocompare.com/data/exchange/histohour?tsym=BTC&limit=10"
    ).then((res) => res.json());
  }
);

const cryptoSingleSlice = createSlice({
  name: "cryptoSingle",
  initialState: {
    cryptoSingleList: [],
    loading: false,
  },
  extraReducers: {
    [getCryptoSingle.pendding]: (state, action) => {
      state.loading = true;
    },
    [getCryptoSingle.fulfilled]: (state, action) => {
      state.loading = false;
      state.cryptoSingleList = action.payload.Data;
      console.log(action.payload.Data.Data);
    },
    [getCryptoSingle.rejected]: (state, action) => {
      state.loading = false;
    },
  },
});
export default cryptoSingleSlice.reducer;
