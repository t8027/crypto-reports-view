import { configureStore } from "@reduxjs/toolkit";

import CryptoRrducer from "./features/cryptoSlice";
import CryptoSingleReducer from './features/cryptoSingleSlice';

export default configureStore({
  reducer: {
    cryptoPair: CryptoRrducer,
    cryptoSingle: CryptoSingleReducer,
  },
});
